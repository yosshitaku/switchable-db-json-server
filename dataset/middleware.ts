import { Request, Response, NextFunction } from 'express';

const jsonServerMiddleware = (req: Request, res: Response, next: NextFunction) => {
    console.log('call middleware')
    next();
}

export default jsonServerMiddleware;