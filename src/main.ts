import path from "path";
import express from "express";
import listDbFiles from "./util/list-db-files";
import serverManager from "./json-server-manager";

const PORT = 3000;
const JSON_SERVER_PORT = 3002;

const app = express();
const manager = serverManager();
app.use(express.json());

app.get("/switch", async (req, res) => {
  const db = req.query.db;

  if (!db || typeof db !== "string") {
    res.status(400).send("Bad Request");
  } else {
    const dbFiles = await listDbFiles();
    const found = dbFiles.find((f) => db === path.parse(f.name).name);
    if (!found) {
      res.status(400).send(`Bad Request. ${db} is not found.`);
    } else {
      await manager.start({ port: JSON_SERVER_PORT, file: found.name });
      res.status(200).send(`Start Json Server with ${found.name} data.`);
    }
  }
});

app.listen(PORT, async () => {
  console.log(`Switchable DB Json Server start on ${PORT}`);
  await manager.start({ port: JSON_SERVER_PORT, file: 'db.json' });
});
