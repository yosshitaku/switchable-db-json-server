import fs from 'fs';
import { Server } from "http";
import jsonServer from "json-server";
import customMiddleware from '../dataset/middleware';

const startJsonServer = async ({ port, file }: { port: number, file: string }) => {
  const app = jsonServer.create();
  const router = jsonServer.router(`dataset/${file}`);
  const middlewares = jsonServer.defaults();
  const routes = await fs.promises.readFile('dataset/routes.json', { encoding: 'utf-8' });

  console.log(routes);

  app.use(middlewares);
  app.use(jsonServer.bodyParser);
  app.use(router);
  app.use(customMiddleware);
  app.use(jsonServer.rewriter(JSON.parse(routes)));

  return app.listen(port, () => {
    console.log(`JSON Server is running on port ${port}.`);
  });
};

const serverManager = () => {
  let server: Server | null = null;

  return {
    start: ({ port, file }: { port: number, file: string }): Promise<void> => {
      return new Promise(async (resolve) => {
        if (server === null || !server.listening) {
          server = await startJsonServer({ port, file });
          resolve();
        } else if (server !== null && server.listening) {
          server.close(async (err) => {
            if (err) {
              throw new Error("Failed to close Json Server.");
            } else {
              server = await startJsonServer({ port, file });
              resolve();
            }
          });
        } else {
          throw new Error("Failed to start Json Server.");
        }
      });
    },
  };
};

export default serverManager;