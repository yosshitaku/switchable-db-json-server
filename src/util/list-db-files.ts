import fs from 'fs';
import path from 'path';

async function listDbFiles() {
    const files = await fs.promises.readdir('./dataset');
    return files.filter(f => {
        return path.parse(f).ext.toLowerCase() === '.json';
    }).map(f => {
        return {
            fullPath: path.resolve(`./dataset/${f}`),
            name: f
        }
    })
}

export default listDbFiles;